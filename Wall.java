package assign1;

import java.math.BigDecimal;

public class Wall {
	//INIT
	private double width;
	private double height;
	//constructors
	Wall(){};
	Wall(double width,double height){
		this.width = width;
		this.height = height;
	}
	//getter
	public double getWidth(){
		return width;
	}
	public double getHeight(){
		return height;
	}
	public double getArea() {
		BigDecimal wdth = new BigDecimal(Double.toString(width));
		BigDecimal hght = new BigDecimal(Double.toString(height));
		return wdth.multiply(hght).doubleValue();
	}
	//setter
	public void setWidth(double width){
		this.width = width < 0 ? 0: width;
	}
	public void setHeight(double height){
		this.height = height < 0 ? 0: height;
	}	
}

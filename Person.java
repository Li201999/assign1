package assign1;

public class Person {
	//INIT
	private String firstName;
	private String lastName;
	private int age;
	//getter
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public int getAge() {
		return age;
	}
	public String getFullName() {
		String str = "";
		if((firstName.isBlank() && firstName.isEmpty())
				&& (lastName.isBlank() && lastName.isEmpty())) {
			return str;
		}else if((firstName.isBlank() && firstName.isEmpty())
				&& !(lastName.isBlank() && lastName.isEmpty())) {
			return lastName;
		}else if(!(firstName.isBlank() && firstName.isEmpty())
				&& (lastName.isBlank() && lastName.isEmpty())) {
			return firstName;
		}else {
			return firstName + " " +lastName;
		}
	}
	
	//setter
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setAge(int age) {
		if (age < 0 || age > 100) {
			this.age = 0;
		}else {
			this.age = age;
		}
	}
	//isTeen
	public boolean isTeen() {
		if(age > 12 && age < 20) {
			return true;
		}else{
			return false;
		}
	}	
}

package assign1;

import java.math.BigDecimal;

public class SimpleCalculator {
	//INIT
	private double firstNumber;
	private double secondNumber;
	//getter
	public double getFirstNumber() {
		return firstNumber;
	}
	public double getSecondNumber() {
		return secondNumber;
	}
	public double getAdditionResult() {
		BigDecimal fNum = new BigDecimal(Double.toString(firstNumber));
		BigDecimal sNum = new BigDecimal(Double.toString(secondNumber));
		return fNum.add(sNum).doubleValue();
	}
	public double getSubtractionResult() {
		BigDecimal fNum = new BigDecimal(Double.toString(firstNumber));
		BigDecimal sNum = new BigDecimal(Double.toString(secondNumber));
		return fNum.subtract(sNum).doubleValue();
	}
	public double getMultiplicationResult() {
		BigDecimal fNum = new BigDecimal(Double.toString(firstNumber));
		BigDecimal sNum = new BigDecimal(Double.toString(secondNumber));
		return fNum.multiply(sNum).doubleValue();
	}
	public double getDivisionResult() {
		if(Math.abs(firstNumber) < 0.0000001 
				|| Math.abs(secondNumber) < 0.0000001) {
			return 0.0;
		}else {
			BigDecimal fNum = new BigDecimal(Double.toString(firstNumber));
			BigDecimal sNum = new BigDecimal(Double.toString(secondNumber));
			return fNum.divide(sNum).doubleValue();
		}
	}
	//setter
	public void setFirstNumber(double firstNumber) {
		this.firstNumber = firstNumber;
	}
	public void setSecondNumber(double secondNumber) {
		this.secondNumber = secondNumber;
	}
}

package assign1;

public class Main {
	public static void main(String[] args) {
		//Person
		System.out.println("----------------------");
		System.out.println("-  Person.Output     -");
		System.out.println("----------------------");
		Person person = new Person();
		person.setFirstName("");
		person.setLastName("");
		person.setAge(10);
		System.out.println("full name = " + person.getFullName());
		System.out.println("teen = " + person.isTeen());
		person.setFirstName("john");
		person.setAge(18);
		System.out.println("full name = " + person.getFullName());
		System.out.println("teen = " + person.isTeen());
		person.setLastName("Smith");
		System.out.println("full name = " + person.getFullName());
		//Sum Calculator class
		System.out.println("----------------------");
		System.out.println("-SumCalculator.Output-");
		System.out.println("----------------------");
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4);
		System.out.println("add = " + calculator.getAdditionResult());
		System.out.println("substract = " + calculator.getSubtractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply = " + calculator.getMultiplicationResult());
		System.out.println("divide = " + calculator.getDivisionResult());
		//Wall Area
		System.out.println("----------------------");
		System.out.println("- Wall Area.Output   -");
		System.out.println("----------------------");
		Wall wall = new Wall(5,4);
		System.out.println("area = " + wall.getArea());
		wall.setHeight(-1.5);
		System.out.println("width = " + wall.getWidth());
		System.out.println("height = " + wall.getHeight());
		System.out.println("area = " + wall.getArea());		
	}
}
